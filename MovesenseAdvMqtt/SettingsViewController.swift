//
//  SettingsViewController.swift
//  MovesenseAdvMqtt
//
//  Created by Eriksson, Timo on 18/12/2017.
//  Copyright © 2017 Movesense. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate  {

    @IBOutlet weak var org: UITextField!
    @IBOutlet weak var accessToken: UITextField!
    @IBOutlet weak var clientId: UITextField!
    @IBOutlet weak var autoReconnect: UISwitch!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var userName: UITextField!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(defaults.string(forKey: "org") != nil){
            org.text = defaults.string(forKey: "org")
        }
        if(defaults.string(forKey: "accessToken") != nil){
            accessToken.text = defaults.string(forKey: "accessToken")
        }
        if(defaults.string(forKey: "clientId") != nil){
            clientId.text = defaults.string(forKey: "clientId")
        }
        if(defaults.string(forKey: "userName") != nil){
            userName.text = defaults.string(forKey: "userName")
        }
        if(defaults.string(forKey: "autoReconnect") != nil){
            autoReconnect.isOn = defaults.bool(forKey: "autoReconnect")
        }
        
        // Do any additional setup after loading the view.
        let version: String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        versionLabel.text = version
        
        self.org.delegate = self;
        self.accessToken.delegate = self;
        self.clientId.delegate = self;
        self.userName.delegate = self;

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didEditUserName(_ sender: Any) {
        defaults.set(userName.text, forKey: "userName")
    }
    @IBAction func didEditOrg(_ sender: Any) {
        defaults.set(org.text, forKey: "org")
    }
    @IBAction func didEditAccessToken(_ sender: Any) {
        defaults.set(accessToken.text, forKey: "accessToken")
    }
    @IBAction func didEditClientId(_ sender: Any) {
        defaults.set(clientId.text, forKey: "clientId")
    }
    @IBAction func didEditAutoReconnect(_ sender: Any) {
        defaults.set(autoReconnect.isOn, forKey: "autoReconnect")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
