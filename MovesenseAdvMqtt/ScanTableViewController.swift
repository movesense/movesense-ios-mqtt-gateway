//
//  ScanTableViewController.swift
//  MovesenseAdvMqtt
//
//  Created by Eriksson, Timo on 18/12/2017.
//  Copyright © 2017 Movesense. All rights reserved.
//

import UIKit
import CoreBluetooth

class Device {
    var name: String?
    var serial: String?
    var last_seen: Int64?
    var uuid: UUID?
    var group: Int?
    var nearby: Bool?
    init(name: String, serial: String, uuid: UUID) {
        self.name = name
        self.serial = serial
        self.uuid = uuid
        self.group = 0
        self.nearby = true
        self.last_seen = Int64(NSDate().timeIntervalSince1970)
    }
}

class Devices {
    var devices = [Device?]()
    
    func add(device: Device) -> Device{
        if let i = devices.index(where: { $0!.uuid == device.uuid}){
            // print("Alredy exists in index = \(i)")
            devices[i]?.last_seen = Int64(NSDate().timeIntervalSince1970)
            devices[i]?.nearby = true
            return(devices[i]!)
        }else{
            device.last_seen = Int64(NSDate().timeIntervalSince1970)
            print("\(device.last_seen!) - Added a new device: UUID = \(device.uuid!)")
            device.nearby = true
            devices.append(device)
            return(device)
        }
    }
    func count(nearby: Bool) -> Int{
        var count: Int = 0
        devices.forEach{
            device in
            if(device!.nearby! == nearby) {
                count = count + 1
            }
        }
        return count
    }
    func get(nearby: Bool, index: Int) -> Device? {
        var count: Int = 0
        for device in devices {
            if(device!.nearby! == nearby) {
                count = count + 1
            }
            if(count == index + 1){
                return device
            }
        }
        return nil
    }
    func updateStatus(){
        let epoch_now = Int64(NSDate().timeIntervalSince1970)
        devices.forEach{
            device in
            if(epoch_now - device!.last_seen!) > 5 {
                if(device?.group != 0){
                    device?.nearby = false
                }else{
                    self.remove(uuid: device!.uuid!)
                }
            }
        }
    }
    func remove(uuid: UUID){
        if let i = devices.index(where: { $0!.uuid == uuid}){
            let epoch_now = Int64(NSDate().timeIntervalSince1970)
            print("\(epoch_now) - Removed a device: UUID = \(uuid)")
            devices.remove(at: i)
        }
    }
    func toggleGroup(device: Device){
        if(device.group! == 0){
            device.group = 1
        }else{
            device.group = 0
        }
    }
}

extension FloatingPoint {
    
    init?(_ bytes: [UInt8]) {
        
        guard bytes.count == MemoryLayout<Self>.size else { return nil }
        
        self = bytes.withUnsafeBytes {
            
            return $0.load(fromByteOffset: 0, as: Self.self)
        }
    }
}
extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-150, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

struct ManufData : Codable {
    var company: UInt32
    var counter: UInt32
    var value: Float32
    var value2: Float32

    init(data: Data){
        self.company = UInt32(data[0]) + UInt32(data[1]) << 8
        self.counter = UInt32(data[3]) + UInt32(data[4]) << 8
        let counter_array : [UInt8] = [UInt8(data[3]),UInt8(data[4]),UInt8(data[5]),UInt8(data[6])]
        let counter_data = Data(bytes: counter_array)
        self.counter = UInt32(littleEndian: counter_data.withUnsafeBytes { $0.pointee })
        if(data.count > 10){
            let value_array : [UInt8] = [UInt8(data[7]),UInt8(data[8]),UInt8(data[9]),UInt8(data[10])]
            self.value = Float32(value_array)!
        } else {
            self.value = 0
        }
        if(data.count > 14){
            let value2_array : [UInt8] = [UInt8(data[11]),UInt8(data[12]),UInt8(data[13]),UInt8(data[14])]
            self.value2 = Float32(value2_array)!
        }else{
            self.value2 = 0
        }
    }

    func toDictionary() -> [String:Any] {
        return ["c":self.counter, "v":self.value, "v2":self.value2]
    }
}

class ScanTableViewController: UITableViewController, CBCentralManagerDelegate {

    var centralManager:CBCentralManager!
    var devices = Devices()
    let defaults = UserDefaults.standard
    var connectionState: String = "Disconnected"
    var gwType: String = "Movesense-Gateway"
    var deviceType: String = "Movesense-Sensor"
    var brokerUrl: String?
    var reconnectDelay: Double = 1

    @IBOutlet weak var buttonConnect: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(defaults.string(forKey: "org") == nil){
            defaults.set("", forKey: "org")
        }
        if(defaults.string(forKey: "accessToken") == nil){
            defaults.set("", forKey: "accessToken")
        }
        if(defaults.string(forKey: "clientId") == nil){
            defaults.set("", forKey: "clientId")
        }
        if(defaults.string(forKey: "userName") == nil){
            defaults.set("", forKey: "userName")
        }
        if(defaults.bool(forKey: "autoReconnect") == nil){
            defaults.set(false, forKey: "autoReconnect")
        }

        self.centralManager = CBCentralManager(delegate: self, queue: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.MqttStateChange), name: NSNotification.Name(rawValue: Mqtt.sharedInst.mqttNotifConnect), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.MqttStateChange), name: NSNotification.Name(rawValue: Mqtt.sharedInst.mqttNotifDisconnect), object: nil)
        
        var timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func update() {
        // print("timer")
        self.devices.updateStatus()
        self.tableView!.reloadData()
    }
    
    @objc func MqttStateChange() {
        print("AutoReconnect = " + String(describing: defaults.bool(forKey: "autoReconnect")))

        if(Mqtt.sharedInst.connected()){
            buttonConnect.title = "Disconnect"
            connectionState = "Connected"
            showToast(message: "Connected")
            reconnectDelay = 1
        }
        else {
            if(connectionState == "Disconnecting"){
                showToast(message: "Disconnected")
                buttonConnect.title = "Connect"
                connectionState = "Disconnected"
            }else if(!defaults.bool(forKey: "autoReconnect")){
                if(connectionState == "Connecting"){
                    showToast(message: "Failed to connect")
                    buttonConnect.title = "Connect"
                    connectionState = "Disconnected"
                }
            }else{
                buttonConnect.title = "Disconnect"
                showToast(message: "Reconnecting")
                connectionState = "Connecting"

                let when = DispatchTime.now() + reconnectDelay
                reconnectDelay = reconnectDelay * 2.0
                if(reconnectDelay > 60){
                    reconnectDelay = 60
                }
                print("reconnectDelay = " + String(describing: reconnectDelay))
                DispatchQueue.main.asyncAfter(deadline: when) {
                    Mqtt.sharedInst.connect()
                }
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ActivateDeactivate(_ sender: Any) {
        if(connectionState == "Connected" || connectionState == "Connecting"){
            buttonConnect.title = "Disconnect"
            connectionState = "Disconnecting"
            Mqtt.sharedInst.disconnect()
        }else if(connectionState == "Disconnected") {
            if(Mqtt.sharedInst.config(org: defaults.string(forKey: "org")!, deviceType: self.gwType, clientId: defaults.string(forKey: "clientId")!, userName: defaults.string(forKey: "userName")!,token: defaults.string(forKey: "accessToken")! )){
                    Mqtt.sharedInst.connect()
                    buttonConnect.title = "Connecting"
                    connectionState = "Connecting"
                    reconnectDelay = 1
            }else{
                showToast(message: "Illegal MQTT client settings")
            }
        }else{
            showToast(message: "Not allowed")
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return("Discovered Devices")
        }else{
            return("Non-Discovered Whitelisted Devices")
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var nearby: Bool?
        if(section == 0){
            nearby = true
        }else{
            nearby = false
        }
        return self.devices.count(nearby: nearby!)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)

        // cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        var nearby: Bool?
        if(indexPath.section == 0){
            nearby = true
        }else{
            nearby = false
        }
        let device = devices.get(nearby: nearby!, index: indexPath.row)!
        cell.textLabel?.text = device.name
        if(device.group == 1){
            cell.backgroundColor = UIColor.red
        }else{
            cell.backgroundColor = UIColor.clear
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row \(indexPath.row) selected")
        var nearby: Bool?
        if(indexPath.section == 0){
            nearby = true
        }else{
            nearby = false
        }
        devices.toggleGroup(device: devices.get(nearby: nearby!, index: indexPath.row)!)
        self.tableView!.reloadData()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        var state = ""
        
        switch central.state {
        case .poweredOn:
            let arrayOfServices: [CBUUID] = [CBUUID(string: "FE06")]
            centralManager.scanForPeripherals(withServices: arrayOfServices, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
            // let arrayOfServices: [CBUUID] = [CBUUID(string: "61353090-8231-49CC-B57A-886370740041")]
            // centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
            state = "Bluetooth LE is turned on and ready for communication."
        case .poweredOff:
            state = "Bluetooth on this device is currently powered off."
        case .unsupported:
            state = "This device does not support Bluetooth Low Energy."
        case .unauthorized:
            state = "This app is not authorized to use Bluetooth Low Energy."
        case .resetting:
            state = "The BLE Manager is resetting; a state update is pending."
        case .unknown:
            state = "The state of the BLE Manager is unknown."
        }
        print(state)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        if let manufData = advertisementData["kCBAdvDataManufacturerData"] as? Data {
            let myManufData = ManufData(data: manufData)
            dump(myManufData)
        }
        if let devName = advertisementData["kCBAdvDataLocalName"] as? String {
            let index = devName.index(devName.endIndex, offsetBy: -12)
            let serial = String(devName[index...])
            let device = self.devices.add(device: Device(name: devName, serial: serial, uuid: peripheral.identifier))
            self.devices.updateStatus()
            // print(serial)
            self.tableView!.reloadData()
            if(Mqtt.sharedInst.connected() && device.group != 0){
                if let manufData = advertisementData["kCBAdvDataManufacturerData"] as? Data {
                    let myManufData = ManufData(data: manufData)
                    let dic = myManufData.toDictionary()
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: dic, options: [])
                        let theJSONText = String(data: jsonData, encoding: .ascii)
                        print("\(serial) sending to MQTT \(theJSONText!)")
                        Mqtt.sharedInst.bmPublish(serial: serial, jsonStr: theJSONText!)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}
