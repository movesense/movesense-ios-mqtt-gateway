//
//  Mqtt.swift
//  movesense-swiftapp
//
//  Created by Eriksson, Timo on 29/08/2017.
//  Copyright © 2017 Suunto. All rights reserved.
//

import Foundation
import CocoaMQTT


class Mqtt {

    static let sharedInst = Mqtt()

    let mqttNotifDisconnect = "com.movesense.mqtt.disconnect"
    let mqttNotifConnect = "com.movesense.mqtt.connect"
    let mqttNotifPing = "com.movesense.mqtt.ping"
    
    let urlPostFix = "messaging.internetofthings.ibmcloud.com"
    
    var mqtt: CocoaMQTT?
    var org: String? = "quickstart"
    var deviceType: String? = "Movesense-Gateway"
    var url: String?
    var userName: String?
    var token: String?
    var clientId: String?
    
    func config(org: String, deviceType: String, clientId: String, userName: String, token: String) -> Bool{
        if(org.count == 0 || clientId.count == 0){
            return(false)
        }
        self.org = org
        if org.range(of:".") != nil {
            self.url = org
            self.userName = userName
        }else{
            self.url = org + "." + urlPostFix
            self.userName = "use-token-auth"
        }
        self.deviceType = deviceType
        self.clientId = clientId
        self.token = token
        return(true)
    }

    func connect() -> Bool{
        if(self.org == nil || self.deviceType == nil || self.clientId == nil){
            return(false)
        }
        let clientID: String = "g:\(String(describing: org!)):\(String(describing: deviceType!)):\(String(describing: clientId!))"
        print("URL: \(url!) Broker URL: \(url!) User Name: \(userName!), Auth Token: \(token!)")
        print("Device Type: " + deviceType! + "Client ID: " + clientID)
        mqtt = CocoaMQTT(clientID: clientID, host: url!, port: 1883)
        mqtt!.username = userName
        mqtt!.password = token
        mqtt!.keepAlive = 0
        mqtt!.delegate = self
        mqtt!.logLevel = CocoaMQTTLoggerLevel.off
        mqtt?.connect()
        return(true)
    }

    func disconnect() {
        mqtt?.disconnect()
    }

    func connected() -> Bool{
        if(mqtt?.connState == CocoaMQTTConnState.connected){
            return(true)
        }else{
            return(false)
        }
    }
    
    func connecting() -> Bool{
        if(mqtt?.connState == CocoaMQTTConnState.connecting){
            return(true)
        }else{
            return(false)
        }
    }
    
    func publish(topic: String, msg: String) -> Bool{
        if(connected()){
            let message = CocoaMQTTMessage(
                topic: topic,
                string: msg,
                qos: CocoaMQTTQOS.qos0
            )
            mqtt!.publish(message)

            return(true)
        }else{
            return(false)
        }
    }

    func subscribe(topic: String) -> Bool{
        if(connected()){
            mqtt!.subscribe(topic)
            return(true)
        }else{
            return(false)
        }
    }
    
    func bmSubscribe(serial: String, cmd: String){
        if(Mqtt.sharedInst.connected()){
            let topic = "iot-2/type/Movesense-Sensor/id/MS-\(serial)/cmd/\(cmd)/fmt/json"
            subscribe(topic: topic)
        }
    }
    
    func bmPublish(serial: String, jsonStr: String){
        if(Mqtt.sharedInst.connected()){
            let topic = "iot-2/type/Movesense-Sensor/id/MS-\(serial)/evt/event/fmt/json"
            let msg = "{\"d\":\(jsonStr)}"
            if(!publish(topic: topic, msg: msg)){
                print("Failed to publish to Bluemix MQTT")
            }
        }
    }
}

extension Mqtt: CocoaMQTTDelegate {
    
    // Optional ssl CocoaMQTTDelegate
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        /// Validate the server certificate
        ///
        /// Some custom validation...
        ///
        /// if validatePassed {
        ///     completionHandler(true)
        /// } else {
        ///     completionHandler(false)
        /// }
        completionHandler(true)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnect host: String, port: Int) {
        print("didConnect: \(host)，port: \(port)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        print("didConnectAck: \(ack)，rawValue: \(ack.rawValue)")
        
        if ack == .accept {
            NotificationCenter.default.post(name: Notification.Name(rawValue: mqttNotifConnect), object: self)
            print("Connection accepted")
        }
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        //print("didPublishMessage with message: \(message.string ?? "")")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("didPublishAck with id: \(id)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        print("didReceivedMessage: \(message.string ?? "") with id \(id)")
        let name = NSNotification.Name(rawValue: "MQTTMessageNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": message.string!, "topic": message.topic])
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
        print("didSubscribeTopic to \(topic)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        print("didUnsubscribeTopic to \(topic)")
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
        print("didPing")
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: mqttNotifPing), object: self)
        _console("didReceivePong")
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: mqttNotifDisconnect), object: self)
        _console("mqttDidDisconnect")
    }
    
    func _console(_ info: String) {
        print("Delegate: \(info)")
    }
}
