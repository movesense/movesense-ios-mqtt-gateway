//
//  MqttViewController.swift
//  movesense-swiftapp
//
//  Created by Eriksson, Timo on 26/08/2017.
//  Copyright © 2017 Suunto. All rights reserved.
//

import Foundation
import UIKit
//import PromiseKit
//import SwiftyJSON
//import Toast_Swift

class MqttViewController : UINavigationControllerDelegate, UITextFieldDelegate
{

    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        NotificationCenter.default.addObserver(self, selector: #selector(MqttViewController.updateMqttStatusLabel), name: NSNotification.Name(rawValue: Mqtt.sharedInst.mqttNotifDisconnect), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MqttViewController.connected), name: NSNotification.Name(rawValue: Mqtt.sharedInst.mqttNotifConnect), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MqttViewController.pingMqtt), name: NSNotification.Name(rawValue: Mqtt.sharedInst.mqttNotifPing), object: nil)
        */
        self.brokerUrl.delegate = self
        if(defaults.string(forKey: "brokerUrl") != nil){
            brokerUrl.text = defaults.string(forKey: "brokerUrl")
        }
        self.authToken.delegate = self
        if(defaults.string(forKey: "authToken") != nil){
            authToken.text = defaults.string(forKey: "authToken")
        }
        self.clientID.delegate = self
        if(defaults.string(forKey: "clientID") != nil){
            clientID.text = defaults.string(forKey: "clientID")
        }
        
        updateMqttStatusLabel()
    }
    
    override func viewDidAppear(_ animated: Bool){
        /*
        self.isConnected = self.movesense!.isDeviceConnected(self.serial!)
        if (self.isConnected) {
            let device = self.movesense!.getDevice(self.serial!)
            let info = device?.deviceInfo;
            if info != nil {
                let str = String(format: "%@ %@\nv%@ %@",
                                 (info?.manufacturerName)!, (info?.productName)!,
                                 (info?.sw)!,(info?.serial)!)
                self.deviceLabel.text = str
            } else {
                self.deviceLabel.text = self.serial
            }
        } else {
            self.deviceLabel.text = self.serial
        }
         */
        super.viewDidAppear(animated)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    func connected() {
        Mqtt.sharedInst.bmSubscribe(serial: self.serial!, cmd: "command")
        updateMqttStatusLabel()
    }

    func updateMqttStatusLabel() {
        if(Mqtt.sharedInst.connected()){
            connectButton.setTitle("Disconnect", for: UIControlState.normal)
            connectButton.backgroundColor = UIColor.green
        }
        else{
            connectButton.setTitle("Connect", for: UIControlState.normal)
            connectButton.backgroundColor = UIColor.darkGray
        }
    }

    func pingMqtt() {
        connectButton.backgroundColor = UIColor.orange
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.updateMqttStatusLabel), userInfo: nil, repeats: false)
        print("ping")
    }

    @IBAction func brokerUrlEdited(_ sender: Any) {
        defaults.set(brokerUrl.text, forKey: "brokerUrl")
    }
    
    @IBAction func authTokenEdited(_ sender: Any) {
        defaults.set(authToken.text, forKey: "authToken")
    }
    
    @IBAction func clientIdEdited(_ sender: Any) {
        defaults.set(clientID.text, forKey: "clientID")
    }

    @IBAction func testEventSend(_ sender: Any) {
        let temp: Int16 = Int16(arc4random_uniform(100)) - 50
        let deviceId: UInt32 = arc4random_uniform(89999999) + 10000000
        //    tmpTopic = "iot-2/evt/update/fmt/json"
        let topic = "iot-2/type/"+self.deviceType+"/id/MS-\(deviceId)/evt/update/fmt/json"
        let msg = "{\"d\":{\"temp\":"+String(temp)+"}}"
        if(Mqtt.sharedInst.publish(topic: topic, msg: msg)){
            print("Publish OK")
        }
    }
    
    @IBAction func connectPressed(_ sender: Any) {
        if(!Mqtt.sharedInst.connected() && !Mqtt.sharedInst.connecting()){
            if(Mqtt.sharedInst.config(url: brokerUrl.text!, deviceType: self.gwType, clientId: clientID.text!, token: authToken.text! )){
                print("Connect OK: " + String(Mqtt.sharedInst.connect()))
                connectButton.setTitle("Connecting", for: UIControlState.normal)
                connectButton.backgroundColor = UIColor.brown
            }
        }else{
            Mqtt.sharedInst.disconnect()
        }
    }
    
}

extension UIViewController {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
